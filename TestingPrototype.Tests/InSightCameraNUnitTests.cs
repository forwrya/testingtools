﻿// Created 2016 by Ryan Forward, Nick Boddy, Jacob Huemann, Aj Leino, and Josh Davies 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Rhino.Mocks;
using MockRepository = Rhino.Mocks.MockRepository;

namespace TestingPrototype.Tests
{
    class InSightCameraNUnitTests
    {
        private InSightCamera _camera;
        private IPEndPoint _default;

        [SetUp]
        public void SetUp()
        {
            _default = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3001);
            _camera = new InSightCamera(_default);
        }

        [TearDown]
        public void TearDown()
        {
            _default = null;
            _camera = null;
        }

        [Test]
        public void TestProcessAddition()
        {
            _camera.ProcessAddition(2, 2);
            Assert.AreEqual("4", _camera.NewestResult);
        }

        [TestCase(Description = "This tests the addition of two negative numbers.")]
        public void TestProcessAdditionNegatives()
        {
            _camera.ProcessAddition(-2, -3);
            Assert.AreEqual("-5", _camera.NewestResult);
        }

        [TestCase(1, -3, "-2")]
        [TestCase(1.5, 4.234, "5.734")]
        public void TestProcessAdditionGeneric(double numOne, double numTwo, string result)
        {
            _camera.ProcessAddition(numOne, numTwo);
            Assert.AreEqual(result, _camera.NewestResult);
        }

        [Test]
        [Explicit("Demonstrates how to ignore a test.")]
        public void TestInvalidConstruction()
        {
            var IPv6Addr = new IPEndPoint(IPAddress.Parse("::1"), 3001);
            Assert.That(() => new InSightCamera(IPv6Addr),
                        Throws.ArgumentException.With.Message.EqualTo("Device must use IPv4"));
        }

        [Test]
        public async Task TestAsyncProcedure()
        {
            DataCollector testCollector = new DataCollector(_camera);
            
            testCollector.ConnectAndSubscribe();
            testCollector.IssueWork(2, 2);
            await testCollector.RetrieveResults();
            Assert.AreEqual("4", testCollector.MostRecentAcquisition);
        }

        [Test]
        public async Task TestProcedureWithMoq()
        {
            var mock = new Mock<InSightAPI>();

            DataCollector testCollector = new DataCollector(mock.Object);

            mock.Setup(camera => camera.Connect(_default)).Returns(true);

            testCollector.ConnectAndSubscribe();
            testCollector.IssueWork(2, 2);
            await testCollector.RetrieveResults();

            mock.Verify(camera => camera.Connect(_default), Times.AtMostOnce);
            mock.Verify(camera => camera.Subscribe(testCollector), Times.AtMostOnce);
            mock.Verify(camera => camera.ProcessAddition(2, 2), Times.AtMostOnce);
        }

        [Test]
        public async Task TestProcedureWithRhinoMocks()
        {
            var mock = MockRepository.GenerateMock<InSightAPI>();
            // Also defines stubs if you just need to force return values

            DataCollector testCollector = new DataCollector(mock);

            mock.Expect(camera => camera.Connect(_default)).Return(true).Repeat.Once();
            mock.Expect(camera => camera.Subscribe(testCollector)).Return(true).Repeat.Once();
            mock.Expect(camera => camera.Trigger()).Return(Task.FromResult<object>(null)).Repeat.Once();

            testCollector.ConnectAndSubscribe();
            testCollector.IssueWork(2, 2);
            await testCollector.RetrieveResults();

            mock.VerifyAllExpectations();
        }

        [Test]
        public void TestReturnTrue()
        {
            var dc = new DataCollector(_camera);

            Assert.IsTrue(dc.ReturnTrue(true));
        }
    }
}