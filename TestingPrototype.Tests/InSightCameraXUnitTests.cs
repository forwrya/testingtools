﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestingPrototype.Tests
{
    public class InSightCameraXUnitTests : IDisposable
    {
        private InSightCamera _camera;
        private IPEndPoint _default;

        public InSightCameraXUnitTests()
        {
            _default = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3001);
            _camera = new InSightCamera(_default);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _default = null;
            _camera = null;
        }

        [Fact]
        public void TestProcessAddition()
        {
            _camera.ProcessAddition(2, 2);
            Assert.Equal("4", _camera.NewestResult);
        }

        [Fact]
        [Trait("Description", "This tests the addition of two negative numbers.")]
        public void TestProcessAdditionNegatives()
        {
            _camera.ProcessAddition(-2, -3);
            Assert.Equal("-5", _camera.NewestResult);
        }
        
        [Theory]
        [InlineData(1, -3, "-2")]
        [InlineData(1.5, 4.234, "5.734")]
        public void TestProcessAdditionGeneric(double numOne, double numTwo, string result)
        {
            _camera.ProcessAddition(numOne, numTwo);
            Assert.Equal(result, _camera.NewestResult);
        }

        [Fact(Skip = "Demonstrates how to ignore a test.")]
        public void TestInvalidConstruction()
        {
            var IPv6Addr = new IPEndPoint(IPAddress.Parse("::1"), 3001);
            Exception ex = Assert.Throws(typeof(ArgumentException), () => new InSightCamera(IPv6Addr));
            Assert.Equal("Device must use IPv4", ex.Message);
        }

        [Fact]
        public async Task TestAsyncProcedure()
        {
            DataCollector testCollector = new DataCollector(_camera);

            testCollector.ConnectAndSubscribe();
            testCollector.IssueWork(2, 2);
            await testCollector.RetrieveResults();
            Assert.Equal("4", testCollector.MostRecentAcquisition);
        }
    }
}
