﻿// Created 2016 by Ryan Forward, Nick Boddy, Jacob Huemann, Aj Leino, and Josh Davies 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestingPrototype
{
    public class InSightCamera : InSightAPI
    {
        private readonly List<DataCollector> _subscribers;

        private readonly IPEndPoint _sensorAddress;

        public InSightCamera(IPEndPoint sensorAddress)
        {
            // Arbitrarily enforce IPv4
            if(sensorAddress.AddressFamily != AddressFamily.InterNetwork)
                throw new ArgumentException("Device must use IPv4");

            _sensorAddress = sensorAddress;
            _subscribers = new List<DataCollector>();
            NewestResult = null;
        }

        /// <inheritdoc />
        public bool Connect(IPEndPoint sensorHostAndPort)
        {
            return sensorHostAndPort.Equals(_sensorAddress);
        }

        /// <inheritdoc />
        public bool Subscribe(DataCollector collector)
        {
            bool subscribed = false;

            if (collector.IsConnected(this))
            {
                _subscribers.Add(collector);
                subscribed = true;
            }

            return subscribed;
        }

        /// <inheritdoc />
        public Task Trigger()
        {
            return Task.Run(() =>
                                {
                                    Thread.Sleep(2000);
                                    foreach (var dataCollector in _subscribers)
                                    {
                                        dataCollector.MostRecentAcquisition = NewestResult;
                                    }
                                });
        }

        /// <inheritdoc />
        public void ProcessAddition(double numOne, double numTwo)
        {
            NewestResult = "" + (numOne + numTwo);
        }

        public string NewestResult { get; private set; }
    }
}