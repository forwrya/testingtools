﻿// Created 2016 by Ryan Forward, Nick Boddy, Jacob Huemann, Aj Leino, and Josh Davies 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TestingPrototype
{
    public class DataCollector
    {
        private InSightAPI _defaultCameraInterface;

        private IPEndPoint _defaultEndPoint;

        private List<InSightAPI> _connectedDevices;

        /// <summary>
        /// Creates a DataCollector that can collect data
        /// from the given InSight device.
        /// </summary>
        /// <param name="cameraInterface"> The InSight device to use in data collection. </param>
        public DataCollector(InSightAPI cameraInterface)
        {
            _defaultCameraInterface = cameraInterface;
            _connectedDevices = new List<InSightAPI>();
            _defaultEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 3001);
        }

        public static void Main(string[] args)
        {
            
        }

        /// <summary>
        /// Connect to default sensor.
        /// </summary>
        public void ConnectAndSubscribe()
        {
            if (_defaultCameraInterface.Connect(_defaultEndPoint))
            {
                _connectedDevices.Add(_defaultCameraInterface);
                _defaultCameraInterface.Subscribe(this);
            }
        }

        /// <summary>
        /// Commands the default camera to do a simple processing task
        /// (adding the two given numbers).
        /// </summary>
        public void IssueWork(double numOne, double numTwo)
        {
            _defaultCameraInterface.ProcessAddition(numOne, numTwo);
        }

        /// <summary>
        /// Retrieves the most recent results from the default camera.
        /// </summary>
        public async Task RetrieveResults()
        {
            await _defaultCameraInterface.Trigger();
        }

        /// <returns> Whether or not this collector is connected to the given sensor. </returns>
        public bool IsConnected(InSightAPI sensor)
        {
            return _connectedDevices.Contains(sensor);
        }

        public bool ReturnTrue(bool tru)
        {
            if (tru)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The most recent result obtained from the sensor.
        /// </summary>
        public string MostRecentAcquisition { get; set; }
    }
}