﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TestingPrototype
{
    public interface InSightAPI
    {
        /// <summary>
        /// Attempt to connect to the sensor at the given
        /// host + port.
        /// </summary>
        /// <param name="sensorHostAndPort"> The host and port of the sensor. </param>
        /// <returns> True if connection was successful, false otherwise. </returns>
        bool Connect(IPEndPoint sensorHostAndPort);

        /// <summary>
        /// Registers the DataCollector to receive results
        /// when the sensor is triggered.
        /// </summary>
        /// <param name="collector"> The DataCollector to which results should be sent. </param>
        /// <returns> True if subscribed, false otherwise. </returns>
        bool Subscribe(DataCollector collector);

        /// <summary>
        /// Triggers the sensor to return the most recent
        /// results as soon as they're available.
        /// </summary>
        /// <returns> A task that returns when each subscriber has received the most recent results. </returns>
        Task Trigger();

        /// <summary>
        /// Represents a simple processing job that can be run on the camera,
        /// in this case adding two numbers.
        /// </summary>
        /// <param name="numOne"> The first number to add. </param>
        /// <param name="numTwo"> The second number to add. </param>
        void ProcessAddition(double numOne, double numTwo);
    }
}
